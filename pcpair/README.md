
# KULLANIMI
# Pcpair çift yönlü bir betiktir. A_Bilgisayarından verilecek örnekler B_Bilgisayarı için de geçerlidir.
# A_Bilgisayarından ;

Medya Server --> 

Rygel medya server her iki bilgisayarda da başlatır. 
A_Bilgisayarında Gupnp Av arayüzü açılır.
Arayüzün içinde her iki tarafın medya (Resim,Müzik,Video) dosyaları görünür.
Seçilen medya, Gerçekleyici alanın da seçilen tarafta oynatılır.


Link Paylaşımı -->

A_Bilgisayarının, wep tarayıcı adres satırılından, adres bağlantısı seçilir.
Link paylaş Tıklanır. Seçilen Link B_Bilgisayarının Varsayılan Wep tarayıcısında açılır.


Ortak Dosyalar -->
 
Pcpair ilk çalıştığında home dizinine Paylaşılanlar adında bir dizin oluşturur.
Bu dizin içerisine atılan her dosya ve dizin, üzerinde yapılan değişikliklerle birlikte diğer tarafa aktarılır.
Herhangi bir dosya, İki taraftan en son hangi tarafta değiştirilmiş ise, o tarafın değişikliği geçerlidir.


Uzak Dizinler -->

A_Bilgisayarından seçildiğin de, Nautilus B_Bilgisayarının HOME/USER dizinin de açılır.


Uzak Terminal -->

A_Bilgisayarından seçildiğin de, Varsayılan Terminal B_Bilgisayarının üzerin de açılır.
Buradan girilen komutlar B_Bilgisayarı için çalışır.
Bu terminalden çıkış için; exit veya logout komutları kullanılabilir.
Bu terminal üzerin de alias kullanılacaksa, alias komuta export DISPLAY değişkeni eklenmelidir. 
Örnek alias aşağıda ki gibi olabilir;

# ~/.bashrc

volumerc() 
{
  export DISPLAY="$DISPLAY" ; local VoLuMe
  
  [[ $1 ]] && VoLuMe="$1" || VoLuMe="90"
  (( $VoLuMe > 200 )) && VoLuMe="200"

    pactl -- set-sink-volume 0 ${VoLuMe}% ; echo $VoLuMe
  }

shutdownrc()
{
	export DISPLAY="$DISPLAY"

trap_shut() { sudo shutdown $arg now ;}

	trap 'trap_shut' 0
	
	[[ $1 ]] && { [[ $1 == +(p|P) ]] && arg=-P || { [[ $1 == +(r|R) ]] && arg=-r || arg=-P ; } ;} || arg=-P
	
	for STP in `awk '{print $1}' < <(wmctrl -l :ACTIVE:)`; do /usr/bin/wmctrl -ic $STP; sleep .5; done

	sleep 1;  exit 0
  }

alias ses=volumerc $@
alias kapat=shutdownrc $@

--------------------------------------------------------------------------------------------------------------------------------------------


# GEREKLİ PAKETLER

"unison" "openssh" "traceroute" "xsel" "glib2" "dbus-glib" "nautilus" "gnome-user-share" "zenity" "rygel" "gupnp-av"

# Arch için;
	sudo pacman -Sy "unison" "openssh" "traceroute" "xsel" "glib2" "dbus-glib" "nautilus" "gnome-user-share" "zenity" "rygel" "gupnp-av" --needed

--------------------------------------------------------------------------------------------------------------------------------------------


# KURULUM

mkdir /tmp/PcPair ; cd /tmp/PcPair ; git clone https://bitbucket.org/andronovo/workspace/src/master/pcpair/ ; 
cd /tmp/PcPair/pcpair/pcpair ; sudo cp -v pcpair /usr/bin ; sudo chmod u+x /usr/bin/pcpair ;
for i in `find . -type f -name '*.desktop'`; do cp -v $i ~/.local/share/applications/; done ; 
cp -v rygel.conf ~/.config/ ; rm -rf /tmp/PcPair ; cd ~

--------------------------------------------------------------------------------------------------------------------------------------------


# YAPILANDIRMA

# sshd servisinin açık olduğunu kontrol et
	systemctl list-unit-files | awk '/sshd.service/{print $0}'
# enable degil ise etkinleştir
  	sudo systemctl enable sshd ; sudo systemctl start sshd


# Bağlantı şekline uygun olarak, IP adreslerini sabitle
	gnome-control-center network
	gnome-control-center wifi


# Nautilus dosya paylaşım ayarlarını aç
	gnome-control-center sharing




# not 1 : Aşağıda ki komutlarda "Enter passphrase (empty for no passphrase)" bölümünü boş bırak
# not 2 : Diğer kullanıcının oturum şifresi sorulacak
# not 3 : Port değeri önceden değiştirilmiş ise, -p 22 değerini buna göre ver.  

# A_Bilgisayarından şu komutları çalıştır
	ssh-keygen -t rsa -b 8192 -o -a 1000 -f ~/.ssh/id_rsa_server
	ssh-copy-id -i ~/.ssh/id_rsa_server.pub -p 22 B_Bilgisayarının_Kullanıcı_Adı@B_Bilgisayarının_IP_Adresi

# B_Bilgisayarından şu komutları çalıştır
	ssh-keygen -t rsa -b 8192 -o -a 1000 -f ~/.ssh/id_rsa_client
	ssh-copy-id -i ~/.ssh/id_rsa_client.pub -p 22 A_Bilgisayarının_Kullanıcı_Adı@A_Bilgisayarının_IP_Adresi




# ssh config dosyasını aç ve aşağıda ki değerleri düzenle
	sudo -H gedit /etc/ssh/sshd_config

# Port değişkeninde diyez var ise, diyezi kaldır ve 0 ile 65535 arası bir değere ayarla (önerilen 1024 üstü bir değer)
# Not : her iki taraf da aynı değer olmalı , Örnek vvv
Port 1923

# X11Forwarding değerinin de diyez var ise, diyezi kaldır ve değerini yes olarak ayarla ; Örnek vvv
X11Forwarding yes

# değişikliklerin etkin olması için;
	sudo systemctl restart sshd




# pcpair içinde , belirtilen 3 alanı uygun şekilde düzenle
	sudo -H gedit /usr/bin/pcpair

# Rygel için aşağıda ki komutu çalıştır ve netwok interface ve paylaşım seçeneklerini düzenle
	rygel-preferences

--------------------------------------------------------------------------------------------------------------------------------------------

