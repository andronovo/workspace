#!/usr/bin/bash

mkdir -p ~/.local/share/Downsubper ; cd ~/.local/share/Downsubper
wget https://raw.githubusercontent.com/emericg/OpenSubtitlesDownload/master/OpenSubtitlesDownload.py
wget https://bitbucket.org/andronovo/workspace/downloads/downsubper
chmod u+x downsubper ; chmod u+x OpenSubtitlesDownload.py ; cd ~/.local/share/applications
wget https://bitbucket.org/andronovo/workspace/downloads/downsubper.desktop
sed -i "s%UserName%$USERNAME%" downsubper.desktop ; cd ~

